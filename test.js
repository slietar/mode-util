/**
 *
 * mode util
 * tests
 *
 **/


import { expect } from 'chai';
import * as modeutil from './lib/mode-util.es2015.js';
import 'source-map-support/register';


let octal = (n) => parseInt(n, 8);


describe('apply()', () => {
  it('should apply modes', () => {
    expect(modeutil.apply('644', '+x')).to.eql(octal(755));
  });

  it('should have a 777 mode by default', () => {
    expect(modeutil.apply('go-x', 'o-w')).to.eql(octal(764));
  });
});

describe('parse()', () => {
  it('should parse add operations', () => {
    expect(modeutil.parse('+x')).to.eql([
      { type: 'or', mode: octal(111) }
    ]);
  });

  it('should parse erase operations', () => {
    expect(modeutil.parse('u=rw')).to.eql([
      { type: 'or', mode: octal(600) },
      { type: 'and', mode: octal(677) }
    ]);
  });

  it('should parse multiple clauses', () => {
    expect(modeutil.parse('o-x,+r')).to.eql([
      { type: 'and', mode: octal(776) },
      { type: 'or', mode: octal(444) }
    ]);
  });

  it('should parse numbers', () => {
    expect(modeutil.parse(octal(655))).to.eql([
      { type: 'and', mode: octal(655) }
    ]);
  });

  it('should parse string numbers as octal numbers', () => {
    expect(modeutil.parse('644')).to.eql([
      { type: 'and', mode: octal(644) }
    ]);
  });

  it('should fail with no operations', () => {
    expect(modeutil.parse.bind(void 0, 'u')).to.throw(/Missing operation/);
  });

  it('should fail with multiple operations', () => {
    expect(modeutil.parse.bind(void 0, '=u-w')).to.throw(/Too many operations/);
  });
});
