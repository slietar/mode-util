/**
 *
 * mode util
 *
 **/


export function apply() {
  let modes = [].slice.call(arguments);

  return modes
    .reduce((ops, source) => {
      return ops.concat(parse(source));
    }, [])
    .reduce((mode, op) => {
      if (op.type === 'and') {
        return mode & op.mode;
      }

      return mode | op.mode;
    }, 511);
}

export function parse(symMode) {
  let operations = [];

  if (typeof symMode === 'number') {
    operations.push({
      type: 'and',
      mode: symMode
    });

    return operations;
  }

  let parsed = parseInt(symMode, 8);

  if (!isNaN(parsed)) {
    operations.push({
      type: 'and',
      mode: parsed
    });

    return operations;
  }

  symMode
    .split(',')
    .forEach((md) => {
      let indexes = ['+', '-', '='].map((char) => md.indexOf(char));
      let sum = indexes.reduce((a, b) => a + b, 0);
      let index = Math.max.apply(Math, indexes);

      if (sum > index - 2) {
        throw new Error('Too many operations');
      } if (index < 0) {
        throw new Error('Missing operation');
      }

      let who = md.slice(0, index);
      let perm = md.slice(index + 1);

      let opType = indexes[0] >= 0
        ? 'add'
        : indexes[1] >= 0
          ? 'remove'
          : 'erase';

      who = who || 'a';

      let dValue = parsePermission(perm);
      let dWho = parseWho(who);

      let value = transformValue(dValue);
      let dMode = repeatValue(value, dWho);
      let mode = transformMode(dMode);

      switch (opType) {
        case 'add':
          operations.push({
            type: 'or',
            mode
          });
          break;
        case 'remove':
          operations.push({
            type: 'and',
            mode: mode ^ 511
          });
          break;
        case 'erase':
          operations.push({
            type: 'or',
            mode
          }, {
            type: 'and',
            mode: transformMode(repeatValue(value ^ 7, dWho)) ^ 511
          });
          break;
      }
    });

  return operations;
}

export function parseMode(mode) {
  let other = mode & 7;
  mode >>= 3;
  let group = mode & 7;
  mode >>= 3;
  let user = mode & 7;

  return {
    user,
    group,
    other
  };
}

export function parsePermission(perm) {
  return {
    read: perm.indexOf('r') >= 0,
    write: perm.indexOf('w') >= 0,
    exec: perm.indexOf('x') >= 0
  };
}

export function parseValue(value) {
  return {
    read: (value & 4) > 0,
    write: (value & 2) > 0,
    exec: (value & 1) > 0
  };
}

export function parseWho(who) {
  let all = who.indexOf('a') >= 0;

  return {
    user: all || who.indexOf('u') >= 0,
    group: all || who.indexOf('g') >= 0,
    other: all || who.indexOf('o') >= 0
  };
}

export function repeatValue(value, dWho) {
  return {
    user: dWho.user ? value : 0,
    group: dWho.group ? value : 0,
    other: dWho.other ? value : 0
  };
}

export function toString(dMode) {
  return [dMode.user, dMode.group, dMode.other].reduce((final, value) => {
    let dValue = parseValue(value);

    return final
      + (dValue.read ? 'r' : '-')
      + (dValue.write ? 'w' : '-')
      + (dValue.exec ? 'x' : '-')
  }, '-');
}

export function transformMode(parsedMode) {
  let mode = 0;

  mode |= parsedMode.user;
  mode <<= 3;
  mode |= parsedMode.group;
  mode <<= 3;
  mode |= parsedMode.other;

  return mode;
}

export function transformValue(dPerm) {
  let value = 0;

  if (dPerm.read) {
    value |= 4;
  } if (dPerm.write) {
    value |= 2;
  } if (dPerm.exec) {
    value |= 1;
  }

  return value;
}
