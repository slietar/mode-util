# mode util

Provides functions to manipulate file modes.

```
The symbolic mode is described by the following grammar:

           mode         ::= clause [, clause ...]
           clause       ::= [who ...] [action ...] action
           action       ::= op [perm ...]
           who          ::= a | u | g | o
           op           ::= + | - | =
           perm         ::= r | s | t | w | x | X | u | g | o

CHMOD(1) BSD General Commands Manual
BSD      July 08, 2004
```


## parse

```js
mode.parse('644');
// => [{ type: 'and', mode: 0644 }]

mode.parse('+x');
// => [{ type: 'or', mode: 0111 }]

mode.parse('go-w');
// => [{ type: 'and', mode: 0755 }]

mode.parse('o=r');
// => [{ type: 'or', mode: 4 }, { type: 'and', mode: 0774 }]

mode.parse('=rw');
// => [{ type: 'or', mode: 666, }, { type: 'and', mode: 666 }]

mode.parse('u=rwx,go=rx');
// => [{ type: 'or', mode: 0700 }, { type: 'and', mode: 0777 }, { type: 'or', mode: 055 }, { type: 'and', mode: 0755 }]

mode.parse('go=');
// => [{ type: 'or', mode: 0 }, { type: 'and', mode: 700 }]

mode.parse('g=u-w');
// => Error: Too many operations
```

To apply modes:

```js
mode.apply('644', '+x');
// => 0755

mode.apply('go-x', 'o-w');
// => 0764
```

When showing modes, don't forget to run `.toString(8)` to make numbers in octal.


## caveats

The parser doesn't support dynamic modes, such as `=u-w` and `go=u-wx`.


## utility functions

### parseMode(mode)

```js
mode.parseMode(0655);
// => { user: 6, group: 5, other: 5 }
```

### parsePermission(perm)

```js
mode.parsePermission('rw');
// => { read: true, write: true, exec: false }
```

### parseValue(value)

```js
mode.parseValue(5);
// => { read: true, write: false, exec: true }
```

### parseWho(who)

```js
mode.parseWho('ug');
// => { user: true, group: true, other: false }

mode.parseWho('a');
// => { user: true, group: true, other: true }
```

### repeatValue(value, dWho)

```js
mode.repeatValue(5, { user: true, group: true, other: false });
// => { user: 5, group: 5, other: 0 }
```

### toString(dMode)

```js
mode.toString({ user: 7, group: 5, user: 5 });
// => '-rwxr-xr-x'

mode.toString({ user: 6, group: 4, user: 4});
// => '-rw-r--r--'
```

### transformMode(dMode)

```js
mode.transformMode({ user: 6, group: 4, user: 4 });
// => 0644
```

### transformValue(dPerm)

```js
mode.transformValue({ read: true, write: true, exec: false });
// => 6
```
